/*
Generate Articles

<article>
    <img src="img/no-img.png" alt="Apfel"/>
    <h4>Apfel</h4>
     <div class="description">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
     </div>
 </article>
*/

var articles = [
    {
        "title": "Apfel",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 2
    },
    {
        "title": "Aubergine",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 1
    },
    {
        "title": "Banane",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 2
    },
    {
        "title": "Brokkoli",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 1
    },
    {
        "title": "Huhn",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 0
    },
    {
        "title": "Orange",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 2
    },
    {
        "title": "Rind",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 0
    },
    {
        "title": "Zucchini",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "",
        "tag": 1
    }
]

var main = document.querySelector("main");

for(var article in articles) {
    article = articles[article];

    var articleElement = document.createElement("article");
    articleElement.setAttribute("data-tag-id", article.tag);

    var img = document.createElement("img");

    if(article.image != "") {
        img.src = article.image;
    }
    else {
        img.src = "img/no-img.png";
    }

    var h4 = document.createElement("h4");
    var t = document.createTextNode(article.title);
    h4.appendChild(t);

    var tagFilterElement = document.querySelector('#filter input[value="' + article.tag +'"]').parentElement;

    var tag = document.createElement("span");

    tag.setAttribute("class",
        tagFilterElement.getAttribute("class")
    );
    var t = document.createTextNode(
        tagFilterElement.querySelector("label").innerHTML
    );
    tag.appendChild(t);

    var description = document.createElement("div");
    description.classList.add("description");
    var t = document.createTextNode(article.description);
    description.appendChild(t);

    var moreLink = document.createElement("a");
    moreLink.classList.add("more");
    moreLink.href = "";
    var t = document.createTextNode("Mehr erfahren");
    moreLink.appendChild(t);

    var tagColor = tagFilterElement.getAttribute("class").split(" ")[1];

    var svg = '<svg height="18px" width="18px" viewBox="0 0 256 256"><path class="' + tagColor + '" d="M76.8,217.6c0-1.637,0.625-3.274,1.875-4.524L163.75,128L78.675,42.925c-2.5-2.5-2.5-6.55,0-9.05s6.55-2.5,9.05,0  l89.601,89.6c2.5,2.5,2.5,6.551,0,9.051l-89.601,89.6c-2.5,2.5-6.55,2.5-9.05,0C77.425,220.875,76.8,219.237,76.8,217.6z"/></svg>';
    moreLink.innerHTML = svg + moreLink.innerHTML;

    description.appendChild(moreLink);

    articleElement.appendChild(img);
    h4.appendChild(tag);
    articleElement.appendChild(h4);
    articleElement.appendChild(description);
    main.appendChild(articleElement);
}


/*
Tag Filter
*/
var  filterInputs = document.querySelectorAll("#filter input");

for(var i = 0; i < filterInputs.length; i++) {
    filterInputs[i].addEventListener("click", function() {
        var activeFilters = getFilterData();
        console.log(activeFilters);

        var articles = document.querySelectorAll("main article");
        for(var i = 0; i < articles.length; i++) {
            if(activeFilters.indexOf(articles[i].getAttribute("data-tag-id")) > -1) {
                articles[i].style.display = 'block';
            }
            else if(activeFilters.length == 0) {
                articles[i].style.display = 'block';
            }
            else {
                articles[i].style.display = 'none';
            }
        }
    });
}

function getFilterData() {
    var checkedFilterInputs = document.querySelectorAll('#filter input:checked')
    var checkedFilters = [];

    for(var i = 0; i < checkedFilterInputs.length; i++) {
        checkedFilters.push(checkedFilterInputs[i].value);
    }

    return checkedFilters;
}


